# MOVIE-DETAILS

***

This  spring-boot application provide the several functionalities :

    * create movies with fields like genre year rating and director
    * create actors with role age for a movie
    * filter movies by their Name
    * filter movies by their genre
    * filter top rated movies
    * create movieUser 
    * review a movie by login 

    
 ##API :

###MOVIE
For POST END-POINT
   * /movie    -   post movie details
   
For GET END-POINT
   * /movie?movieName - get movie by moviename
   * /movie - get all movies
   * /movie/genre - get movie by genre
   * /movie/rating - get high rated movies

For DELETE END-PONT
   * /movie/{id} - delete a particular movie with Id

For PUT END-POINT
   * /movie/{id} - update a movie with Id

###ACTOR

For POST END-POINT
* /actor/{id}    -   post actor details to a movie By movie Id

For GET END-POINT
* /actor - get all actors 

###REVIEW

For POST END_POINT
 * /review/createUser - create a new movieUser
 * /review/{movieId}  - post review for a specific movie

For GET END-POINT

* /review/movie/{movieName} - get review for a movie by movieName

 

     