FROM openjdk:8
ADD target/movie-details-0.0.1-SNAPSHOT.jar movieApp.jar
ENTRYPOINT ["java","-jar","movieApp.jar"]