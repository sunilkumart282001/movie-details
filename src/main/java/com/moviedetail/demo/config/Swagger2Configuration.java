package com.moviedetail.demo.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class Swagger2Configuration {
    @Bean
    public Docket swaggerConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.moviedetail.demo"))
                .build()
                .apiInfo(apiDetails());
    }


    private ApiInfo apiDetails(){
        return new ApiInfo(
                "Movie Details API",
                "API for store retrieve filter movie and their details",
                "1.0",
                "free to use",
                new Contact("sunil","","sunilkumart@gmail.com"),
                "API License",
                "",
                Collections.emptyList()
        );
    }
}
