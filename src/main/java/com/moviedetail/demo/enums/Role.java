package com.moviedetail.demo.enums;

public enum Role {
    DIRECTOR,
    HERO,
    VILLAIN,
    HEROINE,
    COMEDIAN,
    PRODUCER
}
