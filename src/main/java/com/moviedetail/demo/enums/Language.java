package com.moviedetail.demo.enums;

public enum Language {
    TAMIL,
    ENGLISH,
    MALAYALAM,
    TELUGU,
    HINDI,
    KANNADA
}
