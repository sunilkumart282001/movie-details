package com.moviedetail.demo.enums;

public enum Genre {
    ACTION,
    THRILLER,
    COMEDY,
    FICTION
}
