package com.moviedetail.demo.service;

import com.moviedetail.demo.dto.ActorDto;
import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.exception.ResourceNotFoundException;
import com.moviedetail.demo.entity.Actor;
import com.moviedetail.demo.mapper.EntityMapper;
import com.moviedetail.demo.repository.ActorRepository;
import com.moviedetail.demo.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActorService {

    @Autowired
    ActorRepository actorRepository;

    @Autowired
    MovieRepository movieRepository;


    public List<ActorDto> getActors(){
        List<Actor> actorList=actorRepository.findAll();
        if(actorList.isEmpty())
            throw new ResourceNotFoundException("No data found in DB");
        return actorList.stream().map(EntityMapper::ActorToDto).collect(Collectors.toList());


    }


    public ActorDto addWithMovie(ActorDto actorDto,long movieId){
        Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new ResourceNotFoundException("No such ID found"));
        Actor actor = EntityMapper.DtoToActor(actorDto);
        actor.setMovie(movie);
        actorRepository.save(actor);
        return EntityMapper.ActorToDto(actor);
    }

    public List<String> getMovieByActorName(String actorName){
       List<String > MovieList = actorRepository.getMovieWithActorName(actorName);
       if(MovieList.isEmpty())
           throw new ResourceNotFoundException("No Movie found with actorName ");
        return MovieList;
    }




}
