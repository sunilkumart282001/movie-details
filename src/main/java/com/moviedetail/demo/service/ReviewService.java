package com.moviedetail.demo.service;


import com.moviedetail.demo.dto.ReviewRequestDto;
import com.moviedetail.demo.dto.ReviewResponseDto;
import com.moviedetail.demo.dto.UserDto;
import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.entity.MovieUser;
import com.moviedetail.demo.entity.Review;
import com.moviedetail.demo.exception.ResourceNotFoundException;
import com.moviedetail.demo.exception.UnauthorizedUserException;
import com.moviedetail.demo.mapper.EntityMapper;
import com.moviedetail.demo.repository.MovieRepository;
import com.moviedetail.demo.repository.ReviewRepository;
import com.moviedetail.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewService {

    @Autowired
    ReviewRepository reviewRepository;
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    UserRepository userRepository;


    public ReviewResponseDto addWithMovie(ReviewRequestDto reviewRequestDto, long movieId){
        String name = reviewRequestDto.getReviewerName();
        String dtoPassword = reviewRequestDto.getPassword();
        MovieUser movieUser = userRepository.findByUserName(name);
        if(movieUser ==null){
            throw new ResourceNotFoundException("check your movieUser name or create account first");
        }
        String actualPassword = movieUser.getPassword();


        if(movieUser !=null) {
            if (dtoPassword.equals(actualPassword)) {
                Review review = EntityMapper.requestDtoToReview(reviewRequestDto);
                Movie movie = movieRepository.findById(movieId).orElseThrow(() -> new ResourceNotFoundException("No such ID found for movie"));
                review.setMovie(movie);
                reviewRepository.save(review);
            } else {
                throw new UnauthorizedUserException("wrong password");
            }

        }
        else{
            throw new UnauthorizedUserException("check your movieUser name or create account first");
        }
        return EntityMapper.requestToReviewResponseDto(reviewRequestDto);
    }

    public List<ReviewResponseDto> getByMovie(String  movieName){
        List<Review> reviewList=reviewRepository.getByMovieName(movieName);
        if(reviewList.isEmpty())
            throw new ResourceNotFoundException("No data found in DB");

        List<ReviewResponseDto> reviewDtoList = reviewList.stream().map(EntityMapper::ReviewToDto).collect(Collectors.toList());
        return reviewDtoList;
    }

    public String  createUser(UserDto userDto){
       MovieUser movieUser = userRepository.save(EntityMapper.DtoToUser(userDto));

       return "MovieUser Created Successfully!";
    }
}
