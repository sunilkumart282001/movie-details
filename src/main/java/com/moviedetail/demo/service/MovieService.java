package com.moviedetail.demo.service;

import com.moviedetail.demo.dto.MovieDto;
import com.moviedetail.demo.enums.Genre;
import com.moviedetail.demo.exception.ResourceNotFoundException;
import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.mapper.EntityMapper;
import com.moviedetail.demo.repository.MovieRepository;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MovieService {

    @Autowired
    MovieRepository movieRepository;


    static final int DEFAULT_PAGE_INDEX = 0;
    static final int DEFAULT_PAGE_SIZE = 10;
    static final String DEFAULT_SORT_COLUMN = "movieName";

    public Page<MovieDto> getAllMovies(Optional<Integer> pageNo,Optional<Integer> pageSize,Optional<String> sortBy) {
       return movieRepository.findAll(PageRequest.of(pageNo.orElse(DEFAULT_PAGE_INDEX), pageSize.orElse(DEFAULT_PAGE_SIZE), Sort.Direction.ASC, sortBy.orElse(DEFAULT_SORT_COLUMN))).map(EntityMapper::movieToDto);

    }

    public MovieDto add(MovieDto movieDto) {
        Movie movie = movieRepository.save(EntityMapper.DtoToMovie(new Movie(), movieDto));
        return EntityMapper.movieToDto(movie);

    }

    public MovieDto getMovieById(long id) {
        return movieRepository.findById(id).map(EntityMapper::movieToDto).orElseThrow(() -> new ResourceNotFoundException("No data find with ID"));
    }


    public List<MovieDto> getHighRatedMovie() {
        List<Movie> movieList = movieRepository.findHighRatedMovie();
        if (movieList.isEmpty())
            throw new ResourceNotFoundException("No data found in DB");

        return movieList.stream().map(EntityMapper::movieToDto).collect(Collectors.toList());
    }

    public List<MovieDto> getMovieByGenre(Genre genre) {
        List<Movie> movieList = movieRepository.findByGenre(genre);
        if (movieList.isEmpty())
            throw new ResourceNotFoundException("No data found in DB");

        return movieList.stream().map(EntityMapper::movieToDto).collect(Collectors.toList());
    }

    public List<MovieDto> getByMovieName(String movieName) {
        List<Movie> movieList = movieRepository.findByMovieName(movieName);
        if (movieList.isEmpty())
            throw new ResourceNotFoundException("No movie found with the name " + movieName);
        return movieList.stream().map(EntityMapper::movieToDto).collect(Collectors.toList());
    }


    public void deleteMovie(long movieId) throws ResourceNotFoundException {
        Optional<Movie> movie = movieRepository.findById(movieId);
        if (movie.isPresent()) {
            movieRepository.deleteById(movieId);

        } else
            throw new ResourceNotFoundException("No record found with Id " + movieId);


    }

    public MovieDto updateMovie(long movieId, MovieDto movieDto) throws ResourceNotFoundException {
        Optional<Movie> movieUpdate = movieRepository.findById(movieId);
        Movie movie;
        if (movieUpdate.isPresent()) {
           movie =  movieRepository.save(EntityMapper.DtoToMovie(movieUpdate.get(),movieDto));
        }
        else
            throw new ResourceNotFoundException("No record found for movie with Id " + movieId);
     return EntityMapper.movieToDto(movie);

    }
}