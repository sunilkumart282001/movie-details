package com.moviedetail.demo.aop;

import com.moviedetail.demo.exception.ResourceNotFoundException;
import com.moviedetail.demo.exception.UnauthorizedUserException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Aspect
@EnableAspectJAutoProxy
@Configuration
public class AspectConfiguration {
    private Logger log = LoggerFactory.getLogger(AspectConfiguration.class);

    @Pointcut("execution(* com.moviedetail.demo.service.*.*(..))")
    public void PointCut(){}



    @Around("PointCut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {

        log.info("Before method invoked...." + joinPoint.getSignature());
        try {
            long start = System.currentTimeMillis();
            Object  object = joinPoint.proceed();
            long executionTime=System.currentTimeMillis()-start;
            log.info("After method invoked...." + joinPoint.getSignature() +" Executed in " + executionTime + "ms");
            return object;
        } catch (ResourceNotFoundException exception) {
            log.info("Exception occurred: {}", joinPoint);
            log.info("Exception: {} ",exception.getMessage());
           throw new ResourceNotFoundException(exception.getMessage());
        }catch (UnauthorizedUserException exception){
            log.info("Exception occurred: {}", joinPoint);
            log.info("Exception: {} ",exception.getMessage());
            throw new UnauthorizedUserException(exception.getMessage());
        }

    }

    @AfterThrowing("PointCut()")
    public void logAfterThrowing(JoinPoint joinPoint){
        log.info("After throwing exception in: {}",joinPoint);
    }

}
