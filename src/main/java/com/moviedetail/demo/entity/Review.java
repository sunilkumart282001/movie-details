package com.moviedetail.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Review {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Column(name="reviewerName")
    private String reviewerName;
    @Column(name="review")
    @Lob
    private String review;

    @Column(name ="password")
    private String password;

    @ManyToOne
    private Movie movie;


    public Review() {
    }

    public Review(long id, String reviewerName, String review,String password) {
        this.id = id;
        this.reviewerName = reviewerName;
        this.review = review;
        this.password=password;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

     public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonBackReference
    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}

