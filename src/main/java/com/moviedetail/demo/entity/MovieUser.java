package com.moviedetail.demo.entity;

import javax.persistence.*;

@Entity
public class MovieUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "userId")
    private long UserId;
    @Column(name ="username",unique = true)
    private String  userName;
    @Column(name = "password")
    private String  password;


    public MovieUser() {
    }

    public MovieUser(long userId, String userName, String password) {
        UserId = userId;
        this.userName = userName;
        this.password = password;
    }

    public long getUserId() {
        return UserId;
    }

    public void setUserId(long userId) {
        UserId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
