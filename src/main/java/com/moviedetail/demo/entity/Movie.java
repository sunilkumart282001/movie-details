package com.moviedetail.demo.entity;

import com.moviedetail.demo.enums.Genre;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.moviedetail.demo.enums.Language;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.EnumType;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;


import java.util.ArrayList;
import java.util.List;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "movieName",nullable = false,unique = true)
    private String movieName;
    @Column(name="year",nullable = false)
    private long year;
    @Column(name="genre",nullable = false)
    @Enumerated(EnumType.STRING)
    private Genre genre;
    @Column(name="rating")
    private double rating;
    @Column(name="director")
    private String directorName;
    @Column(name ="language")
    @Enumerated(EnumType.STRING)
    private Language language;



    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true,mappedBy = "movie")
    private List<Actor> actors = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name="movie_id",referencedColumnName = "id")
    private List<Review> reviews = new ArrayList<>();

    public Movie() {
    }

    public Movie(long id, String movieName, long year, Genre genre,double rating,String directorName,Language language) {
        this.id = id;
        this.movieName = movieName;
        this.year = year;
        this.genre = genre;
        this.rating=rating;
        this.directorName=directorName;
        this.language=language;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }


    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }


    @JsonManagedReference
    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    @JsonManagedReference
    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }
}
