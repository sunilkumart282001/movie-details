package com.moviedetail.demo.controller;


import com.moviedetail.demo.dto.ActorDto;

import com.moviedetail.demo.service.ActorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;




import java.util.List;

@RestController
@RequestMapping("/actor")
@Api(description ="Actor Management",tags = "Actor")
public class ActorController {
    @Autowired
    ActorService actorService;


    Logger logger = LoggerFactory.getLogger(ActorController.class);


    @PostMapping("/{movieId}")
    @ApiOperation(value = "Create Actor Details",notes = "create new actor associated with movie",tags = {"Actor"})
    @ApiResponses(value ={
            @ApiResponse(code = 201, message = "Actor Created Successfully"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500,message = "Internal Server Error")})
    public ResponseEntity<ActorDto> addWithMovie(@RequestBody ActorDto actorDto, @PathVariable long movieId) {
        logger.trace("Entering Post endpoint in Actor Controller");
        return ResponseEntity.status(HttpStatus.CREATED).body(actorService.addWithMovie(actorDto,movieId));
    }

    @GetMapping
    @ApiOperation(value = "Get All Actor Details",notes = "get all Actor",tags = {"Actor"})
    @ApiResponses(value ={
            @ApiResponse(code = 200, message = "Actor Retrieved Successfully  "),
            @ApiResponse(code = 404, message = "No Actor Found to Display"),
            @ApiResponse(code = 500,message = "Internal Server Error ")})
    public ResponseEntity<List<ActorDto>> getActors() {
        logger.trace("Entering get endpoint in Actor Controller");
        return ResponseEntity.ok().body(actorService.getActors());
    }


     @GetMapping("/{actorName}")
     @ApiOperation(value = "Get All movie by actor ",notes = "get movie by Actor",tags = {"Actor"})
     @ApiResponses(value ={
             @ApiResponse(code = 200, message = "movie Retrieved Successfully  "),
             @ApiResponse(code = 404, message = "No Actor Found to Display"),
             @ApiResponse(code = 500,message = "Internal Server Error ")})
    public ResponseEntity<List<String >> getMovieByActorName(@PathVariable String actorName){
        return ResponseEntity.ok().body(actorService.getMovieByActorName(actorName));
    }


}
