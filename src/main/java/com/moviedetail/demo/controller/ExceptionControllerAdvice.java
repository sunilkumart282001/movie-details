package com.moviedetail.demo.controller;

import com.moviedetail.demo.exception.ErrorResponse;
import com.moviedetail.demo.exception.ResourceNotFoundException;
import com.moviedetail.demo.exception.UnauthorizedUserException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.OffsetDateTime;


@ControllerAdvice
public class ExceptionControllerAdvice  {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> handleResourceNotFoundResponse(ResourceNotFoundException exception){
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimeStamp(OffsetDateTime.now());
        errorResponse.setCode(HttpStatus.NOT_FOUND.value());
        errorResponse.setMessage(exception.getMessage());
        return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handleIllegalArgumentsResponse(MethodArgumentNotValidException exception){
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimeStamp(OffsetDateTime.now());
        errorResponse.setCode(HttpStatus.BAD_REQUEST.value());
        errorResponse.setMessage(exception.getBindingResult().getFieldError().getDefaultMessage());

        return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> handleDataIntegrityViolationResponse(DataIntegrityViolationException exception){
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimeStamp(OffsetDateTime.now());
        errorResponse.setCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        errorResponse.setMessage("Data Already Exist...Please add a new one");

        return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(UnauthorizedUserException.class)
    public ResponseEntity<ErrorResponse> handleUnauthorizedExceptionResponse(UnauthorizedUserException exception){
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setTimeStamp(OffsetDateTime.now());
        errorResponse.setCode(HttpStatus.UNAUTHORIZED.value());
        errorResponse.setMessage(exception.getMessage());

        return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.UNAUTHORIZED);
    }



}
