package com.moviedetail.demo.controller;

import com.moviedetail.demo.dto.ReviewRequestDto;
import com.moviedetail.demo.dto.ReviewResponseDto;
import com.moviedetail.demo.dto.UserDto;
import com.moviedetail.demo.service.ReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/review")
@Api(description ="Review Management",tags = "Review")
public class ReviewController {

    @Autowired
    ReviewService reviewService;

    @PostMapping("/{movieId}")
    @ApiOperation(value = "Create Review for movie",notes = "create new review for movie by movieId",tags = {"Review"})
    @ApiResponses(value ={
            @ApiResponse(code = 201, message = "Review Created Successfully"),
            @ApiResponse(code = 404, message = "Movie Not Found"),
            @ApiResponse(code = 500,message = "Internal Server Error or Movie Name Already Exist")})
    public ResponseEntity<ReviewResponseDto> addWithMovie(@RequestBody ReviewRequestDto reviewRequestDto, @PathVariable long movieId) {
        return ResponseEntity.status(HttpStatus.CREATED).body(reviewService.addWithMovie(reviewRequestDto,movieId));
    }

    @GetMapping("/movie/{movieName}")
    @ApiOperation(value = "Get All Review for Movie",notes = "get all reviews associated with movie",tags = {"Review"})
    @ApiResponses(value ={
            @ApiResponse(code = 200, message = "Reviews Retrieved Successfully  "),
            @ApiResponse(code = 404, message = "No Reviews found to Display or No Movies found to Display"),
            @ApiResponse(code = 500,message = "Internal Server Error ")})
    public ResponseEntity<List<ReviewResponseDto>> getReviewByMovieName(@PathVariable String  movieName){
        return ResponseEntity.status(HttpStatus.OK).body(reviewService.getByMovie(movieName));
    }


    @PostMapping("/createUser")
    @ApiOperation(value = "create user",notes = "create a user",tags = {"Review"})
    @ApiResponses(value ={
            @ApiResponse(code = 201, message = "user created successfully  "),
            @ApiResponse(code = 404, message = "Bad Request"),
            @ApiResponse(code = 500,message = "Internal Server Error ")})
    public ResponseEntity<String > createUser(@Valid @RequestBody UserDto userDto) {
        return ResponseEntity.status(HttpStatus.CREATED).body(reviewService.createUser(userDto));
    }

}
