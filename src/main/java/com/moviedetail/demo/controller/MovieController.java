package com.moviedetail.demo.controller;


import com.moviedetail.demo.dto.MovieDto;
import com.moviedetail.demo.enums.Genre;
import com.moviedetail.demo.service.MovieService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;





import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/movies")
@Api(description ="Movie Management",tags = "Movie")
public class MovieController {

    @Autowired
    MovieService movieService;

    Logger logger = LoggerFactory.getLogger(MovieController.class);


    @PostMapping
    @ApiOperation(value = "Create Movie Details", notes = "create new movie", tags = {"Movie"})
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Movie Created Successfully"),
            @ApiResponse(code = 404, message = "MovieUser Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error or Movie Name Already Exist")})
    public ResponseEntity<MovieDto> add(@Valid @RequestBody MovieDto movieDto) {
        logger.trace("Entering Post endpoint in Movie Controller");
        return ResponseEntity.status(HttpStatus.CREATED).body(movieService.add(movieDto));
    }

    @GetMapping
    @ApiOperation(value = "Get All Movie Details", notes = "get all movies", tags = {"Movie"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Movies Retrieved Successfully  "),
            @ApiResponse(code = 404, message = "No Movies Found to Display"),
            @ApiResponse(code = 500, message = "Internal Server Error ")})
    public ResponseEntity<Page<MovieDto>> getAllMovies(@RequestParam Optional<Integer> pageNo,@RequestParam Optional<Integer> pageSize,@RequestParam Optional<String> sortBy) {
        logger.trace("Entering get endpoint in Movie Controller");
        return ResponseEntity.ok().body(movieService.getAllMovies(pageNo, pageSize,sortBy));
    }


    @GetMapping("/rating")
    @ApiOperation(value = "Get High Rated Movie ", notes = "get all movies which are rated above 9", tags = {"Movie"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "High Rated Movies Retrieved Successfully"),
            @ApiResponse(code = 404, message = "No High Rated Movie Found or No Movies to Display"),
            @ApiResponse(code = 500, message = "Internal Server Error ")})
    public ResponseEntity<List<MovieDto>> findHighRatedMovie() {
        logger.trace("Entering get endpoint in Movie Controller");
        return ResponseEntity.ok().body(movieService.getHighRatedMovie());
    }

    @GetMapping("/genre/{genre}")
    @ApiOperation(value = "Get Movie Details Based on Genre", notes = "filter movies based on genre", tags = {"Movie"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Movies Filtered By Genre Successfully"),
            @ApiResponse(code = 404, message = "Movies Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error ")})
    public ResponseEntity<List<MovieDto>> findMovieByGenre(@PathVariable Genre genre) {
        logger.trace("Entering get endpoint in Movie Controller");
        return ResponseEntity.ok().body(movieService.getMovieByGenre(genre));
    }

    @GetMapping("/")
    @ApiOperation(value = "Get Movie By Name", notes = "get a movie by movie name", tags = {"Movie"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Movie Retrieved By Movie Name Successfully"),
            @ApiResponse(code = 404, message = "No Such Movie Found or Movies Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error ")})
    public ResponseEntity<List<MovieDto>> getMovieByName(@RequestParam String movieName) {
        logger.trace("Entering get endpoint in Movie Controller");
        return ResponseEntity.ok().body(movieService.getByMovieName(movieName));
    }


    @PutMapping("/{movieId}")
    @ApiOperation(value = "Update a Movie", notes = "update a movie by Id", tags = {"Movie"})
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Movie updated Successfully"),
            @ApiResponse(code = 404, message = "No Such Movie Found or Movie Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error")})
    public ResponseEntity<MovieDto> updateMovie(@PathVariable long movieId, @RequestBody MovieDto movieDto) {
        return ResponseEntity.ok().body(movieService.updateMovie(movieId, movieDto));
    }


    @DeleteMapping("/{movieId}")
    @ApiOperation(value = "Delete a Movie", notes = "delete a movie by Id", tags = {"Movie"})
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Movie Deleted Successfully"),
            @ApiResponse(code = 404, message = "No Such Movie Found or Movie Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error ")})
    public ResponseEntity<MovieDto> delete(@PathVariable long movieId) {
        movieService.deleteMovie(movieId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}