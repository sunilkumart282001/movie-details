package com.moviedetail.demo.exception;

import java.time.OffsetDateTime;

public class ErrorResponse {

    private Integer code;
    private String message;
    private OffsetDateTime timeStamp;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OffsetDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(OffsetDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }


}

