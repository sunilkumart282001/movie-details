package com.moviedetail.demo.repository;

import com.moviedetail.demo.entity.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface ActorRepository extends JpaRepository<Actor,Long> {

     @Query(value = "SELECT movie_name FROM movie where id   IN (select movie_id from actor where actor_name =?1)",nativeQuery = true)
     List <String > getMovieWithActorName(String actorName);
}
