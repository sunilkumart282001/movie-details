package com.moviedetail.demo.repository;

import com.moviedetail.demo.entity.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ReviewRepository extends JpaRepository<Review,Long> {
    @Query("SELECT review FROM Review review WHERE review.movie.movieName= ?1")
    List<Review> getByMovieName(String movieName);
}
