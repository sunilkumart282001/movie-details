package com.moviedetail.demo.repository;

import com.moviedetail.demo.entity.MovieUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<MovieUser,Long> {

    @Query("select user from  MovieUser user where user.userName=?1")
    MovieUser findByUserName(String userName);
}
