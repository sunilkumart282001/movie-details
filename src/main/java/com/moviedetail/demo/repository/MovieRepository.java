package com.moviedetail.demo.repository;

import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.enums.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


import java.util.List;

public interface MovieRepository extends JpaRepository<Movie,Long> {

    @Query("SELECT movie FROM Movie movie WHERE movie.rating >=9")
    List<Movie> findHighRatedMovie();
    @Query("SELECT movie FROM Movie movie WHERE movie.genre=?1")
    List<Movie> findByGenre(Genre genre);
    @Query("SELECT movie FROM Movie movie WHERE movie.movieName=?1")
    List<Movie> findByMovieName(String movieName);



}
