package com.moviedetail.demo.dto;

import com.moviedetail.demo.enums.Role;

import javax.persistence.Enumerated;
import javax.persistence.EnumType;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class ActorDto {


    private long actorId;
    @NotBlank(message = "actor name should not be empty")
    private String actorName;
    @NotBlank(message = "role should not be empty")
    @Enumerated(value = EnumType.STRING)
    private Role role;
    @Max(100)@Min(5)
    private int age;


    public ActorDto() {
    }

    public ActorDto(long actorId,String actorName, Role role, int age) {
        this.actorId=actorId;
        this.actorName = actorName;
        this.role = role;
        this.age = age;

    }


    public String getActorName() {
        return actorName;
    }

    public void setActorName(String actorName) {
        this.actorName = actorName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getActorId() {
        return actorId;
    }

    public void setActorId(long actorId) {
        this.actorId = actorId;
    }


}
