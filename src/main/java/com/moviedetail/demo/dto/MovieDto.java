package com.moviedetail.demo.dto;

import com.moviedetail.demo.enums.Genre;
import com.moviedetail.demo.enums.Language;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


public class MovieDto {

    private long movieId;
    @NotBlank(message = "MovieName should not be null")
    private String movieName;
    @NotNull(message ="year must not be null")
    private long year;
    @NotNull(message = "rating should not be empty and not greater than 10")
    @Min(1)@Max(10)
    private double rating;
    @Enumerated(EnumType.STRING)
    private Genre genre;
    @NotBlank(message = "director name should be given")
    private String director;
    @Enumerated(EnumType.STRING)
    private Language language;

    private List<ActorDto> actors = new ArrayList<>();

    public MovieDto() {
    }



    public MovieDto(long movieId, String movieName, long year, Genre  genre,double rating,String director,Language language) {
        this.movieId=movieId;
        this.movieName = movieName;
        this.year = year;
        this.genre = genre;
        this.rating=rating;
        this.director=director;
        this.language=language;
    }


    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public long getYear() {
        return year;
    }

    public void setYear(long year) {
        this.year = year;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre  genre) {
        this.genre = genre;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public long getMovieId() {
        return movieId;
    }

    public void setMovieId(long movieId) {
        this.movieId = movieId;
    }

    public List<ActorDto> getActors() {
        return actors;
    }

    public void setActors(List<ActorDto> actors) {
        this.actors = actors;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

}
