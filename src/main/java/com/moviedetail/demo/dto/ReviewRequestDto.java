package com.moviedetail.demo.dto;


import javax.validation.constraints.NotBlank;

public class ReviewRequestDto {

    @NotBlank(message = "name should not be empty")
    private String reviewerName;
    @NotBlank(message = "review should not be empty")
    private String review;
    @NotBlank(message = "password must be given")
    private String password;

    public ReviewRequestDto() {
    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
