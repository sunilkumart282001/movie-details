package com.moviedetail.demo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class UserDto {

    private long userId;
    @NotBlank(message = "user name should be given")
    private String  userName;
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#&()-[{}]:;',?/*~$^+=<>]).{8,16}$",message = "password doesn't meet the minimum requirement")
    private String  password;

    public UserDto() {
    }

    public UserDto(long userId, String userName, String password) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
