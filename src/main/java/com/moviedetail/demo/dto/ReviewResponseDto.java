package com.moviedetail.demo.dto;

import javax.validation.constraints.NotBlank;


public class ReviewResponseDto {

    @NotBlank(message = "name should not be empty")
    private String reviewerName;
    @NotBlank(message = "review should not be empty")
    private String review;



    public ReviewResponseDto() {
    }


    public ReviewResponseDto(String reviewerName, String review) {
       this.reviewerName = reviewerName;
        this.review = review;

    }

    public String getReviewerName() {
        return reviewerName;
    }

    public void setReviewerName(String reviewerName) {
        this.reviewerName = reviewerName;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

}
