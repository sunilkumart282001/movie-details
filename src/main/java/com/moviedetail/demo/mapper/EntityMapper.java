package com.moviedetail.demo.mapper;

import com.moviedetail.demo.dto.ActorDto;
import com.moviedetail.demo.dto.MovieDto;
import com.moviedetail.demo.dto.ReviewRequestDto;
import com.moviedetail.demo.dto.UserDto;
import com.moviedetail.demo.dto.ReviewResponseDto;
import com.moviedetail.demo.entity.Actor;
import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.entity.MovieUser;
import com.moviedetail.demo.entity.Review;

import java.util.stream.Collectors;

public class EntityMapper {


    public static ActorDto ActorToDto(Actor actor) {


        ActorDto actorDto = new ActorDto();
        actorDto.setActorId(actor.getId());
        actorDto.setActorName(actor.getActorName());
        actorDto.setAge(actor.getAge());
        actorDto.setRole(actor.getRole());
        return actorDto;
    }

    public static Actor DtoToActor(ActorDto actorDto) {


        Actor actor = new Actor();
        Movie movie = new Movie();

        actor.setId(actorDto.getActorId());
        actor.setActorName(actorDto.getActorName());
        actor.setAge(actorDto.getAge());
        actor.setRole(actorDto.getRole());
        actor.setMovie(movie);

        return actor;
    }


    public static MovieDto movieToDto(Movie movie) {

        MovieDto movieDto = new MovieDto();
        movieDto.setMovieId(movie.getId());
        movieDto.setMovieName(movie.getMovieName());
        movieDto.setYear(movie.getYear());
        movieDto.setGenre((movie.getGenre()));
        movieDto.setRating(movie.getRating());
        movieDto.setDirector(movie.getDirectorName());
        movieDto.setLanguage(movie.getLanguage());
       ;movieDto.setActors(movie.getActors().stream().map(EntityMapper::ActorToDto).collect(Collectors.toList()));
        return movieDto;

    }

    public  static Movie DtoToMovie(Movie movie,MovieDto movieDto) {

        movie.setMovieName(movieDto.getMovieName());
        movie.setGenre((movieDto.getGenre()));
        movie.setYear(movieDto.getYear());
        movie.setRating(movieDto.getRating());
        movie.setDirectorName(movieDto.getDirector());
        movie.setLanguage(movieDto.getLanguage());
        return movie;
    }


    public static ReviewResponseDto ReviewToDto(Review review){
        ReviewResponseDto reviewDto = new ReviewResponseDto();
        reviewDto.setReviewerName(review.getReviewerName());
        reviewDto.setReview(review.getReview());

        return reviewDto;
    }


    public static Review requestDtoToReview(ReviewRequestDto reviewRequestDto){
        Review review = new Review();
        review.setReviewerName(reviewRequestDto.getReviewerName());
        review.setReview(reviewRequestDto.getReview());
        return review;
    }

    public static ReviewResponseDto requestToReviewResponseDto(ReviewRequestDto reviewRequestDto){
        ReviewResponseDto reviewResponseDto = new ReviewResponseDto();
        reviewResponseDto.setReview(reviewRequestDto.getReview());
        reviewResponseDto.setReviewerName(reviewRequestDto.getReviewerName());
        return reviewResponseDto;
    }
    public static MovieUser DtoToUser(UserDto userDto){
        MovieUser movieUser = new MovieUser();
        movieUser.setUserId(userDto.getUserId());
        movieUser.setUserName(userDto.getUserName());
        movieUser.setPassword(userDto.getPassword());

        return movieUser;
    }

}
