package com.moviedetail.demo.service;

import com.moviedetail.demo.dto.ActorDto;
import com.moviedetail.demo.entity.Actor;
import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.repository.ActorRepository;
import com.moviedetail.demo.repository.MovieRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.moviedetail.demo.enums.Genre.ACTION;
import static com.moviedetail.demo.enums.Genre.THRILLER;
import static com.moviedetail.demo.enums.Language.KANNADA;
import static com.moviedetail.demo.enums.Language.TAMIL;
import static com.moviedetail.demo.enums.Role.HERO;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


@SpringBootTest(classes = {ActorServiceTest.class})
class ActorServiceTest {

    @Mock
    ActorRepository actorRepository;
    @Mock
    MovieRepository movieRepository;
    @InjectMocks
    ActorService actorService;

    @InjectMocks
    MovieService movieService;

    @Test
    @DisplayName("Test should pass when actor created")
    void test_addActor() {


        ActorDto actorDto = new ActorDto(1,"vijay",HERO,40);
        Actor actor = new Actor(1,"vijay",HERO,40);
        Movie movie = new Movie(4,"mersal",2018,ACTION,8.5,"nelson",TAMIL);
        long id=4;
        when(movieRepository.findById(id)).thenReturn(Optional.of(movie));
        when(actorRepository.save(actor)).thenReturn(actor);
        assertEquals("vijay",actorService.addWithMovie(actorDto,id).getActorName());

    }

    @Test
    @DisplayName("Test should pass when actor retrieved")
    void tes_getActors() {


        ActorDto actorDto = new ActorDto(1,"vijay",HERO,40);
        List<Actor> actors = new ArrayList<>();
        actors.add(new Actor(1,"vijay",HERO,40));
        actors.add(new Actor(1,"vijay",HERO,40));

        when(actorRepository.findAll()).thenReturn(actors);
        assertEquals(2,actorService.getActors().size());

    }

}