package com.moviedetail.demo.service;

import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.entity.Review;
import com.moviedetail.demo.repository.MovieRepository;
import com.moviedetail.demo.repository.ReviewRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.moviedetail.demo.enums.Genre.ACTION;
import static com.moviedetail.demo.enums.Language.TAMIL;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = {ActorServiceTest.class})
class ReviewServiceTest {
    @Mock
    ReviewRepository reviewRepository;

    @Mock
    MovieRepository movieRepository;


    @InjectMocks
    ReviewService reviewService;



    @Test
    @DisplayName("Test should pass when review retrieved")
    void test_getReviewByMovie() {

        List<Review> reviews = new ArrayList<>();
        reviews.add(new Review(1,"Ganesh","good film must watch","Sunil@123"));
        reviews.add(new Review(2,"ram","nice family entertainer","Sunil@123"));

        Movie movie = new Movie(4, "mersal", 2018, ACTION, 8.5, "nelson", TAMIL);
        long id = 4;
        when(movieRepository.findById(id)).thenReturn(Optional.of(movie));
        when(reviewRepository.getByMovieName("mersal")).thenReturn(reviews);
        assertEquals(2 ,reviewService.getByMovie("mersal").size());
    }

    }