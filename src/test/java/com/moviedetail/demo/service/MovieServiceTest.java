package com.moviedetail.demo.service;

import com.moviedetail.demo.dto.MovieDto;
import com.moviedetail.demo.entity.Movie;
import com.moviedetail.demo.mapper.EntityMapper;
import com.moviedetail.demo.repository.MovieRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.moviedetail.demo.enums.Genre.ACTION;
import static com.moviedetail.demo.enums.Genre.THRILLER;
import static com.moviedetail.demo.enums.Language.KANNADA;
import static com.moviedetail.demo.enums.Language.TAMIL;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.verify;


@SpringBootTest(classes = {MovieServiceTest.class})
public class MovieServiceTest {

    @Mock
    MovieRepository movieRepository;

    @InjectMocks
    MovieService movieService;


    @Test
    @DisplayName("Test should pass when movie returned")
    void test_getAllMovies() {

        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1,"KGF",2018,ACTION,9.5,"prashanth",KANNADA));
        movies.add(new Movie(2,"Rocky",2020, THRILLER,8.5,"prashanth",KANNADA));
        Optional<Integer> pageNo= Optional.of(0);
        Optional<Integer> pageSize=Optional.of(10);
        Optional<String> sortBy=Optional.of("movieName");

        when(movieRepository.findAll(PageRequest.of(0,10))).thenReturn(new PageImpl<>(movies));
        assertEquals(2,movieService.getAllMovies(pageNo,pageSize,sortBy).getSize());

    }

    @Test
    @DisplayName("Test should pass when movie is created")
    void test_add() {

         Movie movie = new Movie(3,"Beast",2018,ACTION,9.5,"nelson", TAMIL);
         MovieDto movieDto = new MovieDto(3,"Beast",2018,ACTION,9.5,"nelson", TAMIL);

         mockStatic(EntityMapper.class);

         when(EntityMapper.movieToDto(movie)).thenReturn(movieDto);
         when(movieRepository.save(movie)).thenReturn(movie);
         when(movieService.add(movieDto)).thenReturn(movieDto);
         assertEquals(movie.getMovieName(),movieService.add(movieDto).getMovieName());
    }


    @Test
    void test_getMovieById() {

        Movie movie = new Movie(4,"mersal",2018,ACTION,8.5,"nelson",TAMIL);
        long id =4;

        when(movieRepository.findById(id)).thenReturn(Optional.of(movie));
        assertEquals(id,movieService.getMovieById(id).getMovieId());

    }

    @Test
    void test_getHighRatedMovie() {
         List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1,"KGF",2018,ACTION,9.5,"nelson", TAMIL));

        when(movieRepository.findHighRatedMovie()).thenReturn(movies);
        assertEquals(9.5 ,movieService.getHighRatedMovie().stream().findFirst().get().getRating());
    }

    @Test
    void test_getMovieByGenre() {
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1,"KGF",2018,ACTION,9.5,"nelson",TAMIL));

        when(movieRepository.findByGenre(ACTION)).thenReturn(movies);
        assertEquals(movies.get(0).getGenre(),movieService.getMovieByGenre(ACTION).get(0).getGenre());
    }

    @Test
    void test_getByMovieName() {
        List<Movie> movies = new ArrayList<>();
        movies.add(new Movie(1,"KGF",2018,ACTION,9.5,"nelson",TAMIL));

        when(movieRepository.findByMovieName("KGF")).thenReturn(movies);
        assertEquals(movies.get(0).getMovieName(),movieService.getByMovieName("KGF").get(0).getMovieName());
    }

    @Test
    void test_delete() {
         Movie movie = new Movie(1,"KGF",2018,ACTION,9.5, "nelson",TAMIL);
        long id =1;

        when(movieRepository.findById(id)).thenReturn(Optional.of(movie));
        movieService.deleteMovie(id);
        verify(movieRepository).deleteById(id);
    }

}